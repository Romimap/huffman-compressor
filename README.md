# Huffman Compressor

This compressor uses a huffman tree to encode data.

The material embodied in this software is provided to you "as-is" and without warranty of any kind.

# Usage
Compress a file :
`./huffman -c <filename>`

Decompress a file to stdout :
`./huffman -d <filename>`

Decompress a file to a file :
`./huffman -d <filename> > <newFileName>`

Compress all files in a directory :
`python3 script.py -c <directory>`

Extract and decompress a directory :
`python3 script.py -d <directory>`

# Build from sources
`gcc -o huffman *.c` :-)

# Credits
*BECHEV Boyan & FOURNIER Romain*

*L2,S3, Faculté de Sciences, Universite de Montpellier*