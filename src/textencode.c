/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS //fopen windows
#endif

#include <stdio.h>
#include <stdlib.h>

#include "bintree.h"
#include "ll.h"
#include "maketree.h"
#include "textencode.h"

/* Reads a file and returns a list of nodes, each nodes
 * represents a character and its number of occurences
 * returns a null pointer in case of error
 */
head* func_textencode_toll(char* file_name) {
	int freq[1024] = { 0 };
	FILE *fp = 0;
	char c = '\0';
	head* ans = func_ll_inithead();
	fp = fopen(file_name, "r");
	if (fp == 0) { //security
		printf("cannot open the file\n");
		exit(1);
	}
	//Makes an array freq[char] = frequency
	while (1) {
	  c = fgetc(fp);
	  if (feof(fp)) {
	    break;
	  }
	  freq[(unsigned char)c]++;
	}
	fclose(fp);
	//Reads the array and makes a linked list
	for (int i = 0; i < 1024; i++) {
		if (freq[i]) {
			//adds a node of frequency <freq[i]> and char <i>
			func_ll_add(ans, func_bintree_init(freq[i], (char)i));
		}
	}
	//Prints the linked list
	printf("LinkedList associated with the file...\n");
	func_ll_print(ans);
	return ans;
}

/* to be ignored, debug function
 */
void func_textencode_testreadfile (char* file_name) {
	FILE *fp = 0;
	char c;
	printf("trying to open %s...\n", file_name);
	fp = fopen(file_name, "r");
	if (fp == 0) { //security
		printf("cannot open the file\n");
		return;
	}
	while ((c = fgetc(fp)) != EOF) {
		printf("%c", c);
	}
}
