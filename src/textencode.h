/********************************************************
* File : textencode.h
* Description : Reads a file, creates a linked list based
  on it.
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _TEXTENCODE
#define _TEXTENCODE

#include <stdbool.h>

head* func_textencode_toll(char*);
void func_textencode_testreadfile (char*);

#endif // !_TEXTENCODE
