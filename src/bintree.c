/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bintree.h"
#include "atos.h"

/* Creates a new node, f for frequency, c for the char it represents
 */
node* func_bintree_init (int f, char c) {
  node *n = malloc(sizeof(node));
  n->parent = 0;
  n->left = 0;
  n->right = 0;
  n->frequency = f;
  n->ch = c;
  return n;
}

/* Links a node to another, if there is already a child, link it to the other pointer.
 * returns true if the child has been parented.
 */
bool func_bintree_link (node* parent, node* children) {
  //Checks if there is a free spot for the child
  if (parent->right == 0) { //Right
    parent->right=children;
  } else if (parent->left == 0){ //Left
    parent->left=children;
  } else {
    return false; //if the node already has children
  }
  children->parent = parent;
  parent->frequency += children->frequency;
  return true;
}

/* Tries to link a child to a parent's left or right pointer,
 * returns the left or right pointer of the parent.
 */
node* func_bintree_linklr (node* parent, bool left) {
  if (left) {
    if (parent->left == 0) { //Free spot
      parent->left = func_bintree_init(0, '\0');
      parent->left->parent = parent;
    }
    return parent->left;
  } else {
    if (parent->right == 0) {
      parent->right = func_bintree_init(0, '\0');
      parent->right->parent = parent;
    }
    return parent->right;
  }
}

/* Creates nodes and link them together to create an huffman tree.
 * it converts chars and their encoding into an huffman tree.
 */
void func_bintree_fromcode (node* root, char* code, char ch) {
  int i = 0;
  node* current = root;
  char c;
  c = code[0];
  while (c != '\0') {
      if (c == '0') {
				current = func_bintree_linklr (current, false);
      } else {
				current = func_bintree_linklr (current, true);
      }
      i++;
      c = code[i];
  }
  current->ch = ch;
}

/* Prints a node (char and frequency)
*/
void func_bintree_print (node* leaf) {
  node* current = leaf;
  char* str;
  str = (char*)calloc(6, sizeof(char));
  func_atos_str(str, leaf->ch);
  printf("Char:  %s, ", str);
  printf("Frequency: %d", leaf->frequency);
  printf("\n");
  free(str);
}

/* Prints recursively all leafs
*/
void func_bintree_printr (node* n) {
	if (n->ch != '\0') {
		func_bintree_print(n);
	} else {
		if (n->left)
			func_bintree_printr(n->left);
		if (n->right)
			func_bintree_printr(n->right);
	}
}
