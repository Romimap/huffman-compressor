/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "bintree.h"
#include "ll.h"
#include "atos.h"


/* Creates a new head, requiered to make a list
 */
head* func_ll_inithead () {
  head* h = (head*)malloc(sizeof(head));
  h->c = 0;
  h->len = 0;
  return h;
}

/* Frees a whole list
 */
void func_ll_rmhead(head* h) {
	cell* current = h->c;
	cell* next;
	while (current) {
		next = current->next;
		free(current);
		current = next;
	}
	free(h);
}

/* Creates a new cell, n for data
 */
cell* func_ll_init (node* n) {
  cell* c = (cell*)malloc(sizeof(cell));
  c->next = 0;
  c->prev = 0;
  c->data = n;
  return c;
}

/* Adds a cell to the end of the list
 */
void func_ll_add (head* head, node* n) {
  cell* c = func_ll_init(n);
  cell* current = head->c;

	if (head->len == 0) {//empty list
		head->c = c;
		head->len++;
		return;
	}
  while (current->next) {
    current = current->next;
  }
  head->len++;
  current->next = c;
  c->prev = current;
}

/* Removes a cell from the list;
 */
void func_ll_rm (cell* torm, head* h) {
	if (torm->prev == 0) { //Begining of the list
		//We have to say to the head the new first element, it
		//can be a null pointer or an element.
		h->c = torm->next;
		if (torm->next != 0) {
			//If there is a next element, sets his prev to 0 so
			//we know that it is the new first element.
			torm->next->prev = 0;
		}
	} else { //Anything but the first element
		torm->prev->next = torm->next;
		if (torm->next != 0)//Only if the element isn't the last
			torm->next->prev = torm->prev;
	}
  free(torm);
	h->len--;
}

/* Returns the n cell, or 0 if there is less than n element.
 */
cell* func_ll_get (head* head, int n) {
  if (n >= head->len) {
    return 0;
  }
  cell* current = head->c;
  
  for (int i = 0; i < n && current->next; i++) {
    current = current->next;
  }
  return current;
}

/* Prints the list, len: h->c data: {f0, f1, f2...
 */
void func_ll_print (head* h) {
  cell* current = h->c;
	char* str;
	str = (char*)calloc(6, sizeof(char));
  printf("len: %d data: {", h->len);
  if (current) {
		func_atos_str(str, current->data->ch);
		printf("%s:%d", str, current->data->frequency);
    current = current->next;
  }
  while (current) {
		func_atos_str(str, current->data->ch);
		printf(", %s:%d", str, current->data->frequency);
    current = current->next;
  }
	free(str);
  printf("}\n");
}

/* to be ignored, Debug that prints frequency & cell address
 */
void func_ll_printaddr (head* h) {
	cell* current = h->c;
	printf("{");
	if (current) {
	  printf(" %d  %ld",current->data->frequency, (intptr_t)current);
		current = current->next;
	}
	while (current) {
	  printf("\n, %d  %ld", current->data->frequency, (intptr_t)current);
		current = current->next;
	}
	printf("}\n");
}
