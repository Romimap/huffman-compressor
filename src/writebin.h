/********************************************************
* File : writebin.h
* Description : Utilities to write binary to a file,
* get string arrays as input, puts as many 0 and 1 into
* a char, and once the char is full, store it into the
* file
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _WRITEBIN
#define _WRITEBIN

/*
x represents unwriten data
0100xxxx
*/
typedef struct binindex binindex;
struct binindex {
	int index; //This is where we are on the byte
	unsigned char to_write; //we write on this byte
	FILE* fp;
};

binindex* func_writebin_init(FILE*);
void func_writebin_push(binindex*);
void func_writebin_bit(binindex*, int);
bool func_writebin_write(binindex*, char*);


#endif
