
/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
*
* Description : Reads a file and creates an huffman tree
* to encode it, or reads an encoded file and his header
* to decode it.
*
* Usage : huffman [-c/-d] <file>
*
* Output : an encoded file (.hman)
*	         a decoded file in the terminal
*
* Codes : 			 0 - Everything is fine
*					1 - Cannot open the specified file
*					2 - No chars to encode, maybe the file is empty ?
*					3 - Cannot create/open the new .hman file
*					4 - Missing argument
* 
* Special thanks : my imaginary friend :)
********************************************************/
#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS //fopen windows
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ll.h"
#include "bintree.h"
#include "maketree.h"
#include "textencode.h"
#include "fileencode.h"
#include "decode.h"
#include "writebin.h"

int main (int argc, char** argv) {
  if (argc != 3) {
    printf("Usage : huffman [-c/-d] <filename>\n");
    return (4);
  }
  if (!strcmp(argv[1], "-c")) { //Compressor
    head* h = func_textencode_toll(argv[2]);
    if (h) {
      func_maketree_make(h);
      func_fileencode_encode(argv[2], h->c->data);
    } else {
      return (1);
    }
  } else if (!strcmp(argv[1], "-d")) { //Decompressor
    func_decode_write (argv[2]);
    printf("\n");
  }
  return (0);
}
