/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS //fopen windows
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "bintree.h"
#include "decode.h"
#include "atos.h"

/* Used to transform a byte into a string (the char o represents the byte)
 */
char func_decode_readbit (unsigned char o, unsigned int n) {
  if ((o>>7-n) & 1) {
    return '1';
  }
  return '0';
}

/* Transforms a byte into a string (0b0101000, len = 5) => "01010\0"
 */
char* func_decode_getcode (char o, int len) {
  char* ans = (char*)calloc(len + 1, sizeof(char));
  for (int i = 0; i < len; i++) {
    ans[i] = func_decode_readbit(o, i);
  }
  ans[len] = '\0';
  return ans;
}

/* Inits a treedecode struct
 */
treedecode* func_decode_treedecodeinit (node* root) {
  treedecode* td = (treedecode*)malloc(sizeof(treedecode));
  td->root = root;
  td->current = root;
  return td;
}

/* Travels the tree until it gets to a leaf, returns the char associated with the
 * leaf and start all over again from the root.
 */
char func_decode_runtree (treedecode* td, bool left) {
  if (left) {
    td->current = td->current->left;
  } else {
    td->current = td->current->right;
  }
  if (!td->current->right && !td->current->left) {
    char c = td->current->ch;
    td->current = td->root;
    printf("%c", c);
    return c;
  }
  return '\0';
}

/* Reads the file and create a huffman tree (cf fileencode.h)
 * Then decodes the file
 */
void func_decode_write (char* file_name) {
  //We make an huffman tree from the file
  node* root = func_bintree_init (0, '\0');
  FILE* fp = fopen(file_name, "rb");
  int char_nb = fgetc(fp); //First byte tells us how many chars there is
  char_nb += fgetc(fp); //if there is a char that is equal to 255;
  char ch, len, encoding;
  int tmplen = 0; 
  char* code; //Used to store the encoding as a string of '0' and '1'
  for (int i = 0; i < char_nb; i++) {
    ch = fgetc(fp); //get Char
    len = fgetc(fp); //get Len
    code = (char*)calloc(len + 1, sizeof(char));
    code[0] = '\0';
    while (len > 0) { //get as many byte as nessessary
      encoding = fgetc(fp); //get Code bit
      if (len > 8) { //tells how many bits are to be read from the current byte
	tmplen = 8;
      }
      else {
	tmplen = len;
      }
      strcat(code, func_decode_getcode(encoding, tmplen));
      len -= 8;
    }
    func_bintree_fromcode(root, code, ch);
    free(code);
  }

  //Then we decode the file
  int offset = fgetc(fp);
  treedecode* td = func_decode_treedecodeinit(root);
  char c, c_next;
  int ii = 0;
  c_next = fgetc(fp); //c_next is used to detect the eof one byte in advance
  while (1) {
    c = c_next;
    c_next = fgetc(fp);
    if (feof(fp)) {
      break;
    }
    for (int i = 0; i < 8; i++) {
      func_decode_runtree(td, func_decode_readbit(c, i) == '1');
    }
  }
  for (int i = 0; i < 8 - offset; i++) {
    func_decode_runtree(td, func_decode_readbit(c, i) == '1');
  }
  fclose(fp);
}
