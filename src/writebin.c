/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "writebin.h"

/* Initialise the struct that will be used to push bits
 * into a file.
 */
binindex* func_writebin_init (FILE* fp) {
	binindex* ans = (binindex*)malloc(sizeof(binindex));
	ans->index = 0;
	ans->to_write = 0b00000000;
	ans->fp = fp;
	if (!ans->fp) {
		exit(3);
	}
	return ans;
}

/* write a char into a file and reset the struct
 */
void func_writebin_push (binindex* bin) {
  if (bin->index == 0) //Nothing to push
	  return;
	char c = bin->to_write;
	while (bin->index < 8) { //for the final byte
	    c <<= 1;
	    bin->to_write = c;
	    bin->index++;
	}
	fputc(bin->to_write, bin->fp);
	bin->to_write = 0;
	bin->index = 0;
}

/* fills the "to_write" char, once its full, push it to
 * the file.
 */
void func_writebin_bit (binindex* bin, int value) {
	char c = bin->to_write;
	c <<= 1;
	c += value;
	bin->to_write = c;
	bin->index++;
	if (bin->index == 8) {
		func_writebin_push(bin);
	}
}

/* takes a string, and "converts" it to bits
 * "010111000111" becomes 01011100 0111xxxx
 */
bool func_writebin_write (binindex* bin, char* str) {
	int i = 0;
	while (str[i] != '\0') {
		func_writebin_bit(bin, (str[i] != '0'));
		i++;
	}
}
