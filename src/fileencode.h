/********************************************************                       
* File : fileencode.h                                                             
* Description : Encodes a file based on a text file and
* an huffman tree.
*
* File description:
*                                     ____                           __
* 00000101 00000000 10101010 00000011 11000000 ... 10000010 00000010 10000000
* char nb  char bis char     lenght   encoding     char     lenght   encoding
*                   [                          ...                          ]
* 00000010 01010101 00101011 01010101 ... 10101000
* offset   enc_len  encoded section
*                   [                 ...      ]xx                  
*                                                                               
* Author : FOURNIER Romain, romain.fournier.095@gmail.com     
*	         BECHEV Boyan, boyan.b.2012@feg.bg                  
********************************************************/

#ifndef _FILEENCODE
#define _FILEENCODE

#include "ll.h"
#include "bintree.h"

typedef struct fencode fencode;
struct fencode {
  node* root; //The root of the huffman tree used to encode the file.
  FILE* fp;
};

//Simple struct that can be used to store a char encoding (code t[char])
typedef struct code code;
struct code {
  char* encoding; //Encoding of a char w/o a \0
  int len; //Usefull to know how many bits are to be ignored @ EOF
};

code* func_fileencode_initcode (char*);
fencode* func_fileencode_fencodeinit (char*, node*);
void func_fileencode_fillcodearray (code**, node*, char*, int);
void func_fileencode_printcode (code**);
void func_fileencode_printavglen (code**);
bool func_fileencode_encode (char*, node*);

  
#endif
