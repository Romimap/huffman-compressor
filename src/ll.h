/********************************************************
* File : ll.h
* Description : A linked list implementation to list
* nodes, a head should be created prior to adding
* elements.
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _LL
#define _LL

#include "bintree.h"

typedef struct cell cell;
struct cell {
  cell* next;
  cell* prev;
  node* data;
};

typedef struct head head;
struct head {
  cell* c;
  int len;
};

head* func_ll_inithead ();
void func_ll_rmhead(head*);
cell* func_ll_init (node*);
void func_ll_add   (head*, node*);
void func_ll_rm    (cell*, head*);
cell* func_ll_get  (head*, int);
void func_ll_print (head*);
void func_ll_printaddr (head*);

#endif
