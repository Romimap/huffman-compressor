/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS //fopen windows
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "writebin.h"
#include "ll.h"
#include "bintree.h"
#include "fileencode.h"
#include "atos.h"

/* allocate a code struct
 */
code* func_fileencode_initcode (char* str) {
  code* ans = (code*)malloc(sizeof(code));
  ans->len = strlen(str);
  ans->encoding = (char*)calloc(ans->len + 1, sizeof(char));
  strcpy(ans->encoding, str);
  return ans;
}

/* allocate a fileencode struct
 */
fencode* func_fileencode_fencodeinit (char* file_name, node* root) {
  fencode* ans = (fencode*)malloc(sizeof(fencode));
  ans->fp = fopen(file_name, "r");
  ans->root = root;
  return ans;
}

/* recursively reads the huffman tree and fills "chars" with its data
 * should be called with tmp[256], lvl = 0 and current = root
 */
void func_fileencode_fillcodearray (code** chars, node* current, char* tmp, int lvl) {
  if (!current->left && !current->right) { //No childs
    tmp[lvl] = '\0';
    chars[(unsigned char)current->ch] = func_fileencode_initcode(tmp);
    return;
  }
  if (current->left) {
    tmp[lvl] = '1';
    func_fileencode_fillcodearray (chars, current->left, tmp, lvl + 1);
  }
  if (current->right) {
    tmp[lvl] = '0';
    func_fileencode_fillcodearray (chars, current->right, tmp, lvl + 1);
  }
}

/* Prints the chars array (format c[len]: encoding\n)
 */
void func_fileencode_printcode (code** chars) {
  for (int i = 0; i < 256; i++) {
    if (chars[i]) {
      printf("%d(%d): %s\n", i, chars[i]->len, chars[i]->encoding);
    }
  }
}

/* prints the average encoding len
 */
void func_fileencode_printavglen (code** chars) {
	int all = 0;
	int char_nb = 0;
	for (int i = 0; i < 256; i++) {
		if (chars[i]) {
			all += chars[i]->len;
			char_nb++;
		}
	}
	printf("Avg. encoding: %f\n", (float)all / (float)char_nb);
}

/* Creates an array from the huffman tree and writes it in a file.
 */
bool func_fileencode_encode (char* file_name, node* root) {
  fencode* fe = func_fileencode_fencodeinit(file_name, root);
  code** chars = (code**)calloc(256, sizeof(code*));
  char tmp[256];

  //Stores the huffman tree into an array of code struct
  printf("generating code array...\n");
  func_fileencode_fillcodearray (chars, fe->root, tmp, 0);
  printf("Code array generated !\n");
  func_fileencode_printcode (chars);
	func_fileencode_printavglen(chars);

  //Write in file bit
  char* new_file_name = (char*)calloc(strlen(file_name) + 6, sizeof(char));
  strcpy(new_file_name, file_name);
  strcat(new_file_name, ".hman");

  FILE* new_file = fopen(new_file_name, "w");
  binindex* bin = func_writebin_init(new_file);

  //Stores the array into the .hman file (cf fileencode.h, Line 8)
  //There is 256 chars possible (from 0 to 255), so we have to store the number of chars on 2 bytes.
  unsigned char char_nb = 0;
  unsigned char char_nb_bis = 0;
  for (int i = 0; i < 255; i++) {
    if (chars[i]) {
      char_nb++;
    }
  }
  if (chars[255]) {
    char_nb_bis = 1;
  }

  fputc(char_nb, new_file); 		    //(cf fileencode.h, Line 8, char nb)
  fputc(char_nb_bis, new_file); 	    //(cf fileencode.h, Line 8, char bis)
  for (int i = 0; i < 256; i++) {
    if (chars[i]) {
      fputc((char)i, new_file); 									//(cf fileencode.h, Line 8, char)
      fputc((char)chars[i]->len, new_file); 	    //(cf fileencode.h, Line 8, lenght)
      func_writebin_write(bin, chars[i]->encoding); //(cf fileencode.h, Line 8, encoding)
      func_writebin_push(bin);
    }
  }

  //Stores the actual encoding of the file into the .hman file (cf fileencode.h, Line 10)
  int offset = 0;
  int ch_len = 0;
  FILE* original_file = fopen(file_name, "r");
  char c = 0;
  int iii = 0;

  while (1) {
    /* @ the end of the loop, offset will be equal to the number
     * of bits writen on the last byte
     */
    c = fgetc(original_file);
    if (feof(original_file)) {
      break;
    }
    ch_len = chars[(unsigned char)c]->len;
    offset += ch_len;
    offset %= 8;
  }

	//Now offset is equal to the number of unwriten bits (cf fileencode.h, Line 10, offset)
  offset = 8 - offset;
  if (offset == 8) {
	  offset = 0;
  }
  fputc((unsigned char)offset, new_file);
  fclose(original_file);

  //Encode the original file into the new file
  original_file = fopen(file_name, "r");
  c = 0;
  while (1) {
    //(cf fileencode.h, Line 10, encoded section)
    c = fgetc(original_file);
    if (feof(original_file)) {
      break;
    }
    func_writebin_write(bin, chars[(unsigned char)c]->encoding);
  }
  func_writebin_push(bin);

  //Compare the size of the two files
  int size_o, size_n = 0;
  fseek(original_file, 0, SEEK_END);
  size_o = ftell(original_file);
  fseek(new_file, 0, SEEK_END);
  size_n = ftell(new_file);
  printf("original size : %d; compressed size : %d; ratio : %f !\n"\
	 , size_o, size_n, ((float)size_n)/(float)size_o);

  fclose(original_file);
  fclose(new_file);
  return true;
}
