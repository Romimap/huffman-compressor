/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	   BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "bintree.h"
#include "ll.h"
#include "maketree.h"

/* Creates a huffman tree
 */
bool func_maketree_make(head* head) {
	printf("generating Huffman tree...\n");
	node* min_a = 0;
	node* min_b = 0;
	node* tmp = 0;
	if (head == 0 || head->len == 0) { //Null pointer or empty list
		printf("no chars to encode\n");
		exit(2);
	}
	if (head->len == 1) { //Only one node
		//Creates an huffman tree with only one leaf
		min_a = func_maketree_getmin(head);
		tmp = func_bintree_init(0, '\0');
		func_bintree_link(tmp, min_a);
		func_ll_add(head, tmp);
	}
	while (head->len >= 2) { //While list lenght is has at least 2 cells
		//Gets the smallest nodes from the list and link them together
		min_a = func_maketree_getmin(head);
		min_b = func_maketree_getmin(head);
		tmp = func_bintree_init(0, '\0');
		func_bintree_link(tmp, min_a);
		func_bintree_link(tmp, min_b);
		func_ll_add(head, tmp); //then adds a cell containing the new node to the list
	}
	//Prints the huffman tree
	printf("Huffman tree generated !\n");
	func_bintree_printr(head->c->data);
	return true;
}

/* Removes the cell with the minimum data->frequency
 * from the list and returns it.
 */
node* func_maketree_getmin(head* head) {
	cell* ans = 0;
	node* ansdata = 0;
	if (head == 0 || head->c == 0) { //empty list
		return 0;
	}
	ans = head->c;
	cell* current = ans->next;
	int min = ans->data->frequency;
	while (current) {
		if (current->data->frequency < min) {
			ans = current;
			min = current->data->frequency;
		}
		current = current->next;
	}
	ansdata = ans->data;
	func_ll_rm(ans, head);
	return ansdata;
}
