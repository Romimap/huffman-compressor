/********************************************************
* File : decode.h
* Description : Creates an huffman tree based on a file then decodes
* it.
*
* File description:
* cf. fileencode.h
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _DECODE
#define _DECODE

#include "bintree.h"

typedef struct treedecode treedecode;
struct treedecode {
  node* root;
  node* current;
};

char func_decode_readbit (unsigned char, unsigned int);
char* func_decode_getcode (char, int);
treedecode* func_decode_treedecodeinit (node*);
char func_decode_runtree (treedecode*, bool);
void func_decode_write (char*);

#endif
