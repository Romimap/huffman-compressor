/********************************************************
* File : maketree.h
* Description : Utilities to make an huffman tree based
* on a list of nodes.
*
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _MAKETREE
#define _MAKETREE

bool func_maketree_make(head*);
node* func_maketree_getmin(head*);

#endif // !_MAKETREE
