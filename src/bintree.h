/********************************************************
* File : bintree.h
* Description : Utilities to use a node struct, can be
* used to make a binary tree
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _BINTREE
#define _BINTREE

#include <stdbool.h>

typedef struct node node;
struct node {
  node* parent;
  node* left; //1
  node* right; //0
  int frequency;
  unsigned char ch;
};

node* func_bintree_init (int, char);
bool func_bintree_link (node*, node*);
void func_bintree_fromcode(node*, char*, char);
void func_bintree_print (node*);
void func_bintree_printr (node*);

#endif
