/********************************************************
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS //strncpy windows
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "atos.h"

/* basically an unending switch that makes special chars readable
 */
void func_atos_str(char* str, char c) {
	switch (c) {
	case 0:
		strncpy(str, "NUL", 3);
		break;
	case 1:
		strncpy(str, "SOH", 3);
		break;
	case 2:
		strncpy(str, "STX", 3);
		break;
	case 3:
		strncpy(str, "ETX", 3);
		break;
	case 4:
		strncpy(str, "EOT", 3);
		break;
	case 5:
		strncpy(str, "ENQ", 3);
		break;
	case 6:
		strncpy(str, "ACK", 3);
		break;
	case 7:
		strncpy(str, "BEL", 3);
		break;
	case 8:
		strncpy(str, " BS", 3);
		break;
	case 9:
		strncpy(str, "TAB", 3);
		break;
	case 10:
		strncpy(str, "NL", 2);
		break;
	case 11:
		strncpy(str, "VT", 2);
		break;
	case 12:
		strncpy(str, "NP", 2);
		break;
	case 13:
		strncpy(str, "CR", 2);
		break;
	case 14:
		strncpy(str, "SO", 2);
		break;
	case 15:
		strncpy(str, "SI", 2);
		break;
	case 16:
		strncpy(str, "DLE", 3);
		break;
	case 17:
		strncpy(str, "DC1", 3);
		break;
	case 18:
		strncpy(str, "DC2", 3);
		break;
	case 19:
		strncpy(str, "DC3", 3);
		break;
	case 20:
		strncpy(str, "DC4", 3);
		break;
	case 21:
		strncpy(str, "NAK", 3);
		break;
	case 22:
		strncpy(str, "SYN", 3);
		break;
	case 23:
		strncpy(str, "ETB", 3);
		break;
	case 24:
		strncpy(str, "CAN", 3);
		break;
	case 25:
		strncpy(str, "EM", 2);
		break;
	case 26:
		strncpy(str, "SUB", 3);
		break;
	case 27:
		strncpy(str, "ESC", 3);
		break;
	case 28:
		strncpy(str, "FS", 2);
		break;
	case 29:
		strncpy(str, "GS", 2);
		break;
	case 30:
		strncpy(str, "RS", 2);
		break;
	case 31:
		strncpy(str, "US", 2);
		break;
	case 32:
		strncpy(str, "SPACE", 5);
		break;
	case 127:
		strncpy(str, "DEL", 3);
		break;
	default:
		str[0] = c;
		str[1] = '\0';
		break;
	}
}
