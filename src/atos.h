/********************************************************
* File : atos.h
* Description : Returns ascii chars as strings, for
* example '\n' is returned as "NL" and 'e' as
* "e", it makes special chars readable
* 
* Author : FOURNIER Romain, romain.fournier.095@gmail.com
*	         BECHEV Boyan, boyan.b.2012@feg.bg
********************************************************/
#ifndef _ATOS
#define _ATOS

void func_atos_str(char* str, char);

#endif // !_ATOS
