#!usr/bin/env python3
#
# Author : BECHEV Boyan, boyan.b.2012@feg.bg
#
import sys
import os

# Checks if the script is in the requested directory
def check(repertoire):
    liste = os.listdir(repertoire)
    for fichier in liste:
        if os.path.isdir(repertoire+'/'+fichier):
            check(repertoire+'/'+fichier)
        if os.path.isfile(repertoire+'/'+ fichier):
            if os.path.basename(__file__)==os.path.basename(repertoire+'/'+fichier):
                print("Sorry script is somewhere in the requested directory")
                exit()

# Moves the files to the directory of the huffman program
# and compresses them
def compress(repertoire):
    liste = os.listdir(repertoire)
    for fichier in liste:
        if os.path.isdir(repertoire+'/'+fichier):
            compress(repertoire+'/'+fichier)
        if os.path.isfile(repertoire+'/'+fichier):
            os.system("./huffman -c "+os.path.realpath(repertoire)+'/'+fichier)
            os.system("rm "+repertoire+'/'+fichier)

# Archives all the compressed files into a .tar.gz
def archive(repertoire):
    liste=os.listdir(repertoire)
    for fichier in liste:
        if os.path.isdir(repertoire+'/'+fichier):
            archive(repertoire+'/'+fichier)
        if os.path.isfile(repertoire+'/'+fichier) and os.path.splitext(fichier)[1]==".hman":
            os.system("tar -uf "+sys.argv[2].split("/")[-1]+".tar.gz "+repertoire+'/'+fichier)
            os.system("rm "+repertoire+'/'+fichier)

# Creates a new empty archive
def createArchive():
    os.system("tar -cf "+sys.argv[2].split('/')[-1]+".tar.gz -T /dev/null")

# Extracts an archive
def extract(fichier):
    os.system("tar -xvf "+sys.argv[2].split('/')[-1]+".tar.gz")

# Decompresses all files
def decompress(repertoire):
    liste = os.listdir(repertoire)
    for fichier in liste:
        if os.path.isdir(repertoire+'/'+fichier):
            decompress(repertoire+'/'+fichier)
        if os.path.isfile(repertoire+'/'+fichier):
            os.system("./huffman -d "+os.path.realpath(repertoire)+'/'+fichier+" >"+repertoire+'/'+fichier.rsplit('.',1)[0])
            os.system("rm "+repertoire+'/'+fichier)

if(len(sys.argv)!=3):
    print("Sorry no good args")
    exit()
if(sys.argv[1]=="-c"):
    check(sys.argv[2])
    compress(sys.argv[2])
    createArchive
    archive(sys.argv[2])
elif(sys.argv[1]=="-d"):
    extract(sys.argv[2].split('/')[-1]+".tar.gz")
    decompress(sys.argv[2])
else:
    print("No good option!")
